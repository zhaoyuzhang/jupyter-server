FROM continuumio/anaconda3
MAINTAINER Zhaoyu Zhang

RUN apt-get update -y
RUN apt-get install -yqq gcc
RUN apt-get install -yqq python3-dev libevent-dev
RUN apt-get install -yqq bzip2 build-essential gfortran freetds-dev
RUN pip install pymssql

RUN useradd -ms /bin/bash zhaoyu

ENV HOME /home/zhaoyu
WORKDIR /home/zhaoyu

USER zhaoyu

RUN jupyter notebook --generate-config
RUN echo "c.NotebookApp.port = 8899 " >> /home/zhaoyu/.jupyter/jupyter_notebook_config.py && \
    echo "c.NotebookApp.open_browser = False " >> /home/zhaoyu/.jupyter/jupyter_notebook_config.py  && \
    echo "c.NotebookApp.password = u'sha1:4271dc4ae18d:6e7b3234b8c62a29cf688e93e8b61b3d2aa8665f' " >> /home/zhaoyu/.jupyter/jupyter_notebook_config.py && \
    echo "c.NotebookApp.ip = '*'" >> /home/zhaoyu/.jupyter/jupyter_notebook_config.py && \
    echo "c.NotebookApp.base_url = '/jupyter/'" >> /home/zhaoyu/.jupyter/jupyter_notebook_config.py

EXPOSE 8899
CMD ["jupyter", "notebook", "--no-browser", "--ip=*"]
